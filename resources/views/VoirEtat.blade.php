@extends('VoirFiches')
@section('contenu2')

<form id="formValider" action="{{ route('valider') }}" method="POST">

    @csrf
    <table class="listeLegere">
        <caption>Eléments forfaitisés</caption>
        <thead>
            <tr>
                <th>Id Visiteur</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Mois</th>
                <th>Nb Justif</th>
                <th>Mt Validé</th>
                <th>Date Modif</th>
                <th>Etat Fiche</th>
                <th>Mise en Paiement</th>
            </tr>
        </thead>
        <tbody>
            @foreach($donnees as $donnee)
                <tr>
                    <td class="qteForfait">{{ $donnee->id }}  </td>
                    <td class="qteForfait">{{ $donnee->nom }}</td>
                    <td class="qteForfait">{{ $donnee->prenom }}</td>
                    <td class="qteForfait">{{ $donnee->mois }}</td>
                    <td class="qteForfait">{{ $donnee->nbJustificatifs }}</td>
                    <td class="qteForfait">{{ $donnee->montantValide }}</td>
                    <td class="qteForfait">{{ $donnee->dateModif }}</td>
                    <td class="qteForfait">{{ $donnee->idEtat }}</td>
                    <td><input type="checkbox" name="ids[]" value="{{ $donnee->id }}"></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <button type="submit">Valider</button>
</form>




<div id="pagination">
    <ul class="pagination"></ul>
    <button id="prevBtn" class="btn btn-primary">Précédent</button>
    <button id="nextBtn" class="btn btn-primary">Suivant</button>
</div>

<div id="validerContainer" style="position: relative;">
    <button id="validerBtn" class="btn btn-success" style="position: absolute; bottom: 0; right: 0;">Valider</button>
    <span id="checkedCount" style="position: absolute; bottom: 0; right: 50px;"></span>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
    // Fonction pour diviser les données en pages
    function paginateData(data, pageSize) {
        var paginatedData = [];
        for (var i = 0; i < data.length; i += pageSize) {
            paginatedData.push(data.slice(i, i + pageSize));
        }
        return paginatedData;
    }

    // Fonction pour afficher les données paginées
    function displayData(page) {
        $('#pagination .pagination li').removeClass('active');
        $('#pagination .pagination li').eq(page).addClass('active');

        var start = page * 10;
        var end = start + 10;
        $('table.listeLegere tbody tr').hide().slice(start, end).show();
        $('table.listeLegere thead').show(); // Afficher les en-têtes du tableau
    }

    // Initialiser la pagination
    var pageCount = Math.ceil({{ $donnees->count() }} / 10);
    var paginationHtml = '';
    for (var i = 0; i < pageCount; i++) {
        paginationHtml += '<li class="page-item"><a class="page-link" href="#" data-page="' + i + '">' + (i + 1) + '</a></li>';
    }
    $('#pagination .pagination').html(paginationHtml);

    // Afficher la première page de données
    displayData(0);

    // Gérer le clic sur la pagination
    $('#pagination .pagination').on('click', 'a', function(e) {
        e.preventDefault();
        var page = parseInt($(this).data('page'));
        displayData(page);
    });

    // Gérer le clic sur le bouton suivant
    $('#nextBtn').click(function() {
        var activePage = $('#pagination .pagination li.active').index();
        var nextPage = activePage + 1;
        if (nextPage < pageCount) {
            displayData(nextPage);
        }
    });

    // Gérer le clic sur le bouton précédent
    $('#prevBtn').click(function() {
        var activePage = $('#pagination .pagination li.active').index();
        var prevPage = activePage - 1;
        if (prevPage >= 0) {
            displayData(prevPage);
        }
    });

    // Cacher les boutons précédent/suivant s'il n'y a pas plus de 10 pages
    if (pageCount <= 1) {
        $('#prevBtn, #nextBtn').hide();
    }

    // Afficher les boutons précédent/suivant par groupe de 10
    $('#pagination .pagination li').hide().slice(0, 10).show();
    $('#nextBtn').click(function() {
        var lastVisiblePage = $('#pagination .pagination li:visible:last').index();
        $('#pagination .pagination li').hide().slice(lastVisiblePage + 1, lastVisiblePage + 11).show();
    });

    $('#prevBtn').click(function() {
        var firstVisiblePage = $('#pagination .pagination li:visible:first').index();
        $('#pagination .pagination li').hide().slice(firstVisiblePage - 9, firstVisiblePage + 1).show();
    });

});

// Gérer le clic sur le bouton Valider
$('#validerBtn').click(function() {
        var checkedBoxes = $('table.listeLegere tbody input[type="checkbox"]:checked');
        var ids = checkedBoxes.map(function() {
            return $(this).data('id');
        }).get();

        // Envoi des données au serveur pour la mise à jour
        $.ajax({
            url: '{{ route("valider") }}',
            method: 'POST',
            data: { ids: ids },
            success: function(response) {
                // Gérer la réponse du serveur, par exemple, actualiser la page ou afficher un message de succès.
                console.log(response);
            },
            error: function(xhr, status, error) {
                // Gérer les erreurs, par exemple, afficher un message d'erreur.
                console.error(error);
            }
        });
    });


    $('#validerBtn').click(function() {
        var checkedBoxes = $('table.listeLegere tbody input[type="checkbox"]:checked');
        var checkedCount = checkedBoxes.length;
        $('#checkedCount').text(checkedCount + " case(s) cochée(s) validée(s)");
    });



</script>

@endsection

