@extends('sommairecomptable')
@section('contenu1')

<div id="contenu">
        <h2>Les fiches de frais </h2>
        <h3>Sélectionner : </h3>

        @php
            $laLigne = ['CL','CR','RB','VA']; // Déclaration de la variable $laLigne comme un tableau vide
        @endphp
                
      <form action="{{ route('chemin_VoirEtat') }}" method="post">
        
        {{ csrf_field() }} <!-- laravel va ajouter un champ caché avec un token -->
        <div class="corpsForm"><p>
          <label for="idEtat" >Etat des fiches de frais : </label>
          <select id="idEtat" name="idEtat">
    @foreach($laLigne as $etat)
        <option value="{{ $etat }}">
            {{ $etat }}
        </option>
    @endforeach
</select>

</div>

        <div class="piedForm">
        <p>
          <input id="ok" type="submit" value="Valider" size="20" />
          <input id="annuler" type="reset" value="Effacer" size="20" />
        </p> 
        </div>
          
        </form>



@endsection