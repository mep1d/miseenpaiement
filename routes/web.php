<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

        /*-------------------- Use case connexion---------------------------*/
Route::get('/',[ //ce que la personne voit//
        'as' => 'chemin_connexion',
        'uses' => 'connexionController@connecter'
]);

Route::post('/',[
        'as'=>'chemin_valider',
        'uses'=>'connexionController@valider'
]);
Route::get('deconnexion',[
        'as'=>'chemin_deconnexion',
        'uses'=>'connexionController@deconnecter'
]);

         /*-------------------- Use case état des frais---------------------------*/
Route::get('selectionMois',[
        'as'=>'chemin_selectionMois',
        'uses'=>'etatFraisController@selectionnerMois'
]);

Route::post('listeFrais',[
        'as'=>'chemin_listeFrais',
        'uses'=>'etatFraisController@voirFrais'
]);

        /*-------------------- Use case gérer les frais---------------------------*/

Route::get('gererFrais',[
        'as'=>'chemin_gestionFrais',
        'uses'=>'gererFraisController@saisirFrais'
]);

Route::post('sauvegarderFrais',[
        'as'=>'chemin_sauvegardeFrais',
        'uses'=>'gererFraisController@sauvegarderFrais'//signifie que qd l'utilisateur a cliqué sur valider, le code qui va s'afficher se trouve dans ce controller//
]);

Route::get('proto',[
        'as'=>'chemin_proto',
        'uses'=>'protoController@voirDernierFrais'
]);

/*------------------------ Use case comptable--------------------*/

Route :: get('sommairecomptable',[
        'as' => 'chemin_sommairecomptable',
        'uses'=> 'comptableController@sommairecomptable'
        
]);


Route::get('VoirFiches', [
        'as'=> 'chemin_suividesfiches',
        'uses'=>'comptableController@VoirFiches'
]);

  
Route::post('VoirEtat', [
        'as'=> 'chemin_VoirEtat',
        'uses'=>'comptableController@RecupEtat'
]);
Route::get('VoirEtat', [
        'as'=> 'chemin_VoirEtat',
        'uses'=>'comptableController@RecupEtat'
]);

Route::get('pagination', [comptableController::class, 'index'])->name('pagination');

/*Route::post('pagination', [
        'as' => 'chemin_VoirEtat',
        'uses' => 'comptableController@pagination'
    ]);*/
    
        
        



Route :: get ('edition', [
        'as' => 'chemin_edition',
        'uses'=> 'comptableController@edition'
        ]);
        

// Modifier la route pour la validation des fiches de frais
Route::post('/valider-fiches', [comptableController::class, 'valider'])->name('valider');

// Conserver la route pour afficher la vue de validation des fiches de frais
Route::get('/resultat', [comptableController::class, 'showValidationForm'])->name('resultat');


        
