<?php

namespace App\Http\Controllers;
use App\MyApp\PdoGsb;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Views\Modeles;
use Illuminate\Support\Collection;



class comptableController extends Controller


{
    function sommairecomptable(){
        $comptable = session('comptable');
        return view('sommairecomptable', ['comptable' => $comptable]);

    }


    public function VoirFiches(Request $request) {
        $comptable = session('comptable');
        //$laLigne = $request['idEtat'];
    // Utilisation de la méthode getIdEtat() de la classe PdoGsb pour récupérer les états
    $pdo = new pdogsb();
    $laLigne = $pdo->getIdEtat();
   
    //dd($laLigne);
    return view('VoirFiches', compact('comptable', 'laLigne'));
    }
    



    function edition(){
        $comptable = session('comptable');
        return view('edition', ['comptable' => $comptable] );

    }


    /*public function RecupEtat(Request $request)
    {   $comptable = session('comptable');
        // Récupérer l'id de l'état sélectionné depuis le formulaire
        $idEtat = $request->input('idEtat');
    
        // Appeler la méthode statique du modèle pour récupérer les données
        $donnees = PdoGsb::RecupEtat($idEtat);
        $items = $donnees->paginate(5);

       
        
    
        // Récupérer les valeurs individuelles
        $idVisiteur = $donnees->pluck('id');
        $nomV = $donnees->pluck('nom');
        $prenomV = $donnees->pluck('prenom');
        $mois = $donnees->pluck('mois');
        $nbJustif = $donnees->pluck('nbJustificatifs');
        $montantValide = $donnees->pluck('montantValide');
        $dateModif = $donnees->pluck('dateModif');
        $idEtat = $donnees->pluck('idEtat');
        //dd($idVisiteur, $nomV, $prenomV, $mois, $idEtat);
        // Retourner la vue avec les données
        return view('VoirEtat', compact('idVisiteur', 'nomV', 'prenomV', 'mois', 'idEtat','comptable', 'nbJustif','montantValide', 'dateModif', 'items'));
    }*/
    


   /* public function pagination(Request $request)
{
    // Récupérer l'id de l'état sélectionné depuis la requête
    $idEtat = $request->input('idEtat');
    
    // Appeler la méthode du modèle pour récupérer les résultats paginés
    $donnees = PdoGsb::RecupEtat($idEtat);
    
    // Paginer les résultats
    $resultats = $donnees->paginate(10);

    // Retourner les résultats paginés au format JSON
    return response()->json($resultats);
}*/

/*public function index(Request $request)
{
    $items = PdoGsb::paginate(5);

    if ($request->ajax()) {
        return view('VoirEtat', compact('items'));
    }

    return view('VoirEtat',compact('items'));
}*/

public function RecupEtat(Request $request)
{
    $comptable = session('comptable');
    // Récupérer l'id de l'état sélectionné depuis le formulaire
    $idEtat = $request->input('idEtat');

    // Appeler la méthode statique du modèle pour récupérer les données
    $donnees = PdoGsb::RecupEtat($idEtat);

    // Convertir la collection en un objet Paginator pour la pagination
    $items = new Paginator($donnees, 5);

    // Retourner la vue avec les données
    return view('VoirEtat', compact('donnees', 'comptable', 'items'));
}




  // Afficher la vue de validation des fiches de frais
  public function showValidationForm()
  {
      $fiches = FicheFrais::all(); // Récupérer toutes les fiches de frais
      return view('view', compact('fiches'));
  }

 // Modifier la méthode valider pour rediriger vers la vue 'resultat'
public function valider(Request $request)
{
    $ids = $request->input('ids');

    // Validation de chaque fiche de frais sélectionnée
    foreach ($ids as $id) {
        $fiche = FicheFrais::find($id);
        if ($fiche) {
            // Mettre à jour l'état et la date de modification de la fiche
            $fiche->idEtat = 'RB'; // ID de l'état "Remboursée"
            $fiche->dateModif = date('Y-m-d'); // Date actuelle au format AAAA-MM-JJ
            $fiche->save();
        }
    }

    // Réponse de redirection vers la vue 'resultat' avec un message de succès
    $count = count($ids);
    return redirect()->route('resultat')->with('success', "Vous avez validé $count fiches de frais avec succès.");
}

}   

    

        



