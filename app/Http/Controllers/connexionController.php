<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PdoGsb;

class connexionController extends Controller
{
    function connecter(){
        
        return view('connexion')->with('erreurs',null);
    } 
    function valider(Request $request){
        $login = $request['login'];
        $mdp = $request['mdp'];
        $visiteur = PdoGsb::getInfosVisiteur($login,$mdp);
        $comptable = PdoGsb :: get_InfosComptable($login,$mdp);
        if(!is_array($visiteur) && !is_array($comptable)){//si ce visiteur n'est pas dans le tableau: je créée une liste d'erreurs //
            $erreurs[] = "Login ou mot de passe incorrect(s)";
            return view('connexion')->with('erreurs',$erreurs);}

         if(is_array($comptable))  {

            session(['comptable' => $comptable ]);
           return view ('sommairecomptable') ->with ('comptable', session('comptable'));
                             }                         
        
        if (is_array($visiteur)){

            session(['visiteur' => $visiteur]);
            return view('sommaire')->with('visiteur',session('visiteur'));
        }
    }

    function deconnecter(){
            session(['visiteur' => null]);
            return redirect()->route('chemin_connexion');
    }
       
}
